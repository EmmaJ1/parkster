﻿using Parkster.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Parkster.Domain.Services
{
    public interface IParkingService
    {
        Task<IEnumerable<Parking>> GetParkingsByVehicleId(int vehicleId);               //lista samtliga parkeringar för ett visst fordon
        Task<Parking> StartParking(int vehicleId, int locationId, DateTime startDate);       //registrera en parkering
        //Task<Parking> EndParking(string registrationNumber);                              //Tar bort den pga saknas UserStory
    }
}
