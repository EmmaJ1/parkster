﻿using Parkster.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Parkster.Domain.Services
{
    public interface IVehicleService
    {
        Task<Vehicle> CreateVehicleAsync(string registrationNumber, string owner);
        Task<bool> DeleteVehicleAsync(int id);
    }
}
