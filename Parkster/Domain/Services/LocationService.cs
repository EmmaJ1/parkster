﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Parkster.Data;
using Parkster.Domain.Entities;

namespace Parkster.Domain.Services
{
    public class LocationService : ILocationService
    {
        private readonly ParksterContext context;

        public LocationService(ParksterContext context)
        {
            this.context = context;
        }



        public async Task<IEnumerable<Location>> GetLocationsAsync()
        {
            return await context.Locations.ToListAsync();

        }
        
        public async Task<Location> CreateLocationAsync(string name, decimal pricePerMinute)
        {
            var newLocation = new Location
            {
                Name = name,
                PricePerMinute = pricePerMinute
            };

            context.Locations.Add(newLocation);
            await context.SaveChangesAsync();

            return newLocation;

        }
    }
}
