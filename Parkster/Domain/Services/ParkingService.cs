﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Parkster.Data;
using Parkster.Domain.Entities;

namespace Parkster.Domain.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ParksterContext context;

        public ParkingService(ParksterContext context)
        {
            this.context = context;
        }
      

        public Task<IEnumerable<Parking>> GetParkingsByVehicleId(int vehicleId)
        {
            throw new NotImplementedException();
        }


        public async Task<Parking> StartParking(int vehicleId, int locationId, DateTime startDate)
        {
            var newParking = new Parking
            {
                VehicleId = vehicleId,
                LocationId = locationId,
                StartDate = startDate
                
            };

            context.Parkings.Add(newParking);
            await context.SaveChangesAsync();

            return newParking;


            //throw new NotImplementedException();
        }

        //public Task<Parking> EndParking(string registrationNumber)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
