﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Parkster.Data;
using Parkster.Domain.Entities;

namespace Parkster.Domain.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly ParksterContext context;

        public VehicleService(ParksterContext context)
        {
            this.context = context;
        }


        public async Task<Vehicle> CreateVehicleAsync(string registrationNumber, string owner)
        {
            var newVehicle = new Vehicle
            {
                RegistrationNumber = registrationNumber,
                Owner = owner
            };

            context.Vehicles.Add(newVehicle);
            await context.SaveChangesAsync();

            return newVehicle;

        }

        public async Task<bool> DeleteVehicleAsync(int id)
        {
            var vehicle = context.Vehicles.FirstOrDefault(x => x.Id == id);

            if (vehicle == null)
            {
                return await Task.FromResult(false);
            }

            context.Vehicles.Remove(vehicle);
            var recordsAffected = await context.SaveChangesAsync();    

            return recordsAffected > 0;         

        }
    }
}
