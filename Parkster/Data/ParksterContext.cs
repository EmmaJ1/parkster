﻿using Microsoft.EntityFrameworkCore;
using Parkster.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Parkster.Data
{
    public class ParksterContext : DbContext
    {
        public ParksterContext(DbContextOptions<ParksterContext> options) : base(options) 
        {

        }

        public DbSet<Location> Locations { get; set; }                  

        public DbSet<Parking> Parkings { get; set; }

        public DbSet<Vehicle> Vehicles { get; set; }
    }
}
