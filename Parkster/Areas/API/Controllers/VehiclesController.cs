﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Parkster.Data;
using Parkster.Domain.Entities;
using Parkster.Domain.Services;

namespace Parkster.Areas.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IVehicleService vehicleService;

        public VehiclesController(IVehicleService vehicleService)
        {
            this.vehicleService = vehicleService;
        }



        //private readonly ParksterContext _context;

        //public VehiclesController(ParksterContext context)
        //{
        //    _context = context;
        //}

        //// GET: api/Vehicles
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Vehicle>>> GetVehicles()
        //{
        //    return await _context.Vehicles.ToListAsync();
        //}

        //// GET: api/Vehicles/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<Vehicle>> GetVehicle(int id)
        //{
        //    var vehicle = await _context.Vehicles.FindAsync(id);

        //    if (vehicle == null)
        //    {
        //        return NotFound();
        //    }

        //    return vehicle;
        //}

        //// PUT: api/Vehicles/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutVehicle(int id, Vehicle vehicle)
        //{
        //    if (id != vehicle.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(vehicle).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!VehicleExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/Vehicles
        [HttpPost]
        public async Task<IActionResult> PostVehicle(Vehicle vehicle)
        {
            var newVehicle = await vehicleService.CreateVehicleAsync(vehicle.RegistrationNumber, vehicle.Owner);

            return Created($"/api/vehicles/{newVehicle.Id}", newVehicle);






            //_context.Vehicles.Add(vehicle);           //orig-kod
            //await _context.SaveChangesAsync();

            //return CreatedAtAction("GetVehicle", new { id = vehicle.Id }, vehicle);
        }

        // DELETE: api/Vehicles/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVehicle(int id)
        {
            var deletedVehicle = await vehicleService.DeleteVehicleAsync(id);

            if (deletedVehicle)
            {
                return NoContent();
            }

            return NotFound();





            //var vehicle = await _context.Vehicles.FindAsync(id);
            //if (vehicle == null)
            //{
            //    return NotFound();
            //}

            //_context.Vehicles.Remove(vehicle);
            //await _context.SaveChangesAsync();

            //return vehicle;
        }

        //private bool VehicleExists(int id)
        //{
        //    return _context.Vehicles.Any(e => e.Id == id);
        //}
    }
}
